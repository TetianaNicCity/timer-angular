import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent implements OnInit {
  @Output() title = new EventEmitter<boolean>();

  timeLeft = 10;
  interval;
  showTime: boolean;
  changeTitle: boolean;
  disablePause = true;
  constructor() {}

  ngOnInit() {}
  startTimer() {
    this.title.emit((this.changeTitle = false));
    this.showTime = true;
    this.disablePause = false;
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.showTime = false;
        this.pauseTimer();
        this.timeLeft = 10;
        this.disablePause = true;
        this.title.emit((this.changeTitle = true));
      }
    }, 1000);
  }

  pauseTimer() {
    clearInterval(this.interval);
  }
}
