import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit {
  newtitle = 'Title';
  show: boolean;

  constructor() {}

  ngOnInit() {
    this.show = false;
  }
  display(title) {
    title ? this.newtitle = 'New title' :  this.newtitle = 'Title will change';
    this.show = title;
  }
}
