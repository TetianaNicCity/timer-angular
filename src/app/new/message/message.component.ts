import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
  @Input() show: boolean;
  showMessage: boolean;

  constructor() {}

  ngOnInit() {
    this.showMessage = true;
  }
  closeMessage() {
    this.showMessage = false;
  }
}
