import { Component, OnInit } from '@angular/core';
import { EndTimeService } from '../endtimer.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
  endTime: boolean;
  showMessage: boolean;


  constructor(private endTimeService: EndTimeService) {}

  ngOnInit() {
    this.endTimeService.endTime.subscribe((show: boolean) => {
      this.endTime = show;
    })
  }
  closeMessage() {
    this.endTime = false;
  }
}
