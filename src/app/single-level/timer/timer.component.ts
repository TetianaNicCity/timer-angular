import { Component, OnInit } from '@angular/core';
import { EndTimeService } from '../endtimer.service';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent implements OnInit {
  title: string;
  timeLeft = 10;
  interval;
  showTime: boolean;
  changeTitle: boolean;
  disablePause = true;
  constructor(private endTimeServise: EndTimeService) {}

  ngOnInit() {
    this.title = "Timer";
  }
  startTimer() {
    this.endTimeServise.setEndTime(false);
    this.title = "Timer start";
    this.showTime = true;
    this.disablePause = false;
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.showTime = false;
        this.pauseTimer();
        this.timeLeft = 10;
        this.disablePause = true;
        this.endTimeServise.setEndTime(true);
        this.title = "Timer end";
      }
    }, 1000);
  }

  pauseTimer() {
    clearInterval(this.interval);
  }
}
