import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
 providedIn: 'root'
})
export class EndTimeService {
 constructor() { }
 public endTime = new Subject();

 setEndTime(loggedIn: boolean) {
   this.endTime.next(loggedIn);
 }
}