import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NewComponent } from './new/new.component';
import { TitleComponent } from './new/title/title.component';
import { MessageComponent } from './new/message/message.component';
import { TimerComponent } from './single-level/timer/timer.component';
import { ReportComponent } from './single-level/report/report.component';

@NgModule({
  declarations: [AppComponent, NewComponent, TitleComponent, MessageComponent, TimerComponent, ReportComponent],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
